/**
 * Created by Rok on 26.7.2015.
 */

// Test to see if the code is running on a touch device
function cssAnimation(legendId) {
    var isTouchSupported = 'ontouchstart' in window.document;
    this.defaultValues = {
        numCircles: '300',
        minSize: '50',
        maxSize: '300'
    };
    this.continue = true;
    this.count = 0;
    this.fpsMeterRunning = false;
    this.nuclearWeaponsOn = false;
    this.events = {
        onDown : isTouchSupported ? 'touchstart' : 'mousedown',
        onMove : isTouchSupported ? 'touchmove' : 'mousemove',
        onEnd  : isTouchSupported ? 'touchend' : 'mouseup'
    };
    this.size = {
        width: document.body.clientWidth,
        height: document.body.clientHeight
    };
    this.supportedTransform = cssAnimation._getBrowserVendorTransform();
    this._setUpCSSAnimation();

    this._generateLegend(legendId);
    this._registerEventListeners();
    window.stats = new Stats('deviceInfo', 'container', this.defaultValues);
}

cssAnimation.prototype._setUpCSSAnimation = function() {
    var keyframePrefix = getAnimationKeyframePrefix(),
        style = document.createElement('style');
    style.type = 'text/css';

    document.head.appendChild(style);
    var stylesheet = document.styleSheets[0];

    stylesheet.insertRule(
        '@' + keyframePrefix + 'keyframes animateBoxy {' +
            'from { ' + this.supportedTransform + ': ' + 'translateX(0px); } ' +
            'to { ' + this.supportedTransform + ': ' + 'translateX(' + (this.size.width <= this.defaultValues.maxSize ? 0 : this.size.width - this.defaultValues.maxSize) + 'px); }' +
        '}', stylesheet.rules.length
    );

    // Borrowed from MDN: https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Animations/Detecting_CSS_animation_support
    function getAnimationKeyframePrefix() {
        var animation = false,
        animationString = 'animation',
        keyframePrefix = '',
        domPrefixes = 'Webkit Moz O ms Khtml'.split(' '),
        pfx = '',
        elm = document.createElement('div');

        if (elm.style.animationName !== undefined) {
            animation = true;
        }

        if (animation === false) {
          for (var i = 0; i < domPrefixes.length; i++) {
            if (elm.style[ domPrefixes[i] + 'AnimationName' ] !== undefined) {
              pfx = domPrefixes[ i ];
              animationString = pfx + 'Animation';
              keyframePrefix = '-' + pfx.toLowerCase() + '-';
              animation = true;
              break;
            }
          }
        }

        return keyframePrefix;
    }
};

// Borrowed from: http://stackoverflow.com/questions/5661671/detecting-transform-translate3d-support?answertab=votes#tab-top
cssAnimation._getBrowserVendorTransform = function() {
    if (!window.getComputedStyle) {
        return false;
    }

    var el = document.createElement('p'),
        has3d,
        supportedTransform = null,
        transforms = {
            'transform': 'transform',
            'webkitTransform': '-webkit-transform',
            'OTransform': '-o-transform',
            'msTransform': '-ms-transform',
            'MozTransform': '-moz-transform'
        };

    // Add it to the body to get the computed style.
    document.body.insertBefore(el, null);

    for (var t in transforms) {
        if (el.style[t] !== undefined) {
            el.style[t] = "translate3d(1px,1px,1px)";
            has3d = window.getComputedStyle(el).getPropertyValue(transforms[t]);
            if (has3d !== undefined && has3d.length > 0 && has3d !== "none") {
                supportedTransform = t;
                break;
            }
        }
    }

    document.body.removeChild(el);

    return supportedTransform;
};

cssAnimation.prototype._registerEventListeners = function() {
    document.getElementById('startButton').addEventListener(this.events.onDown, this.start.bind(this));
    document.getElementById('stopButton').addEventListener(this.events.onDown, this.stopGenerateCircles.bind(this));
    document.getElementById('continueButton').addEventListener(this.events.onDown, this.continueGenerateCircles.bind(this));
    document.getElementById('restartButton').addEventListener(this.events.onDown, this.restartExperiment.bind(this));

    if (!window.FPSMeter) {
        throw new Error('FPSMeter is not present in window scope');
    } else {
        document.addEventListener('fps', function(ev) { document.getElementById('fpsCounter').innerText = ev.fps; });
    }
};

cssAnimation.prototype._generateLegend = function(legendId) {
    var html = '<span id="fpsCounter">0</span> FPS<br />' +
        '<span id="elementsCounter">0</span> elements on screen' +
        '<hr>' +
        'Enter details:<br />' +
        '<input type="text" id="deviceInfo" placeholder="Test device">' +
        '<input type="text" id="numCircles" placeholder="Number of circles">' +
        '<input type="text" id="minSize" placeholder="Min Size">' +
        '<input type="text" id="maxSize" placeholder="Max Size">' +
        '<button id="startButton" style="float: left">Start</button>' +
        '<button id="stopButton" style="float: left; display: none">Stop</button>' +
        '<button id="continueButton" style="float: left; display: none">Continue</button>' +
        '<button id="restartButton" style="float: right">Restart</button>';

    document.getElementById(legendId).innerHTML = html;
};

cssAnimation.prototype.start = function(ev) {
	this.count = 0;
	this.continue = true;
	ev.target.style.display = 'none';
	document.getElementById('stopButton').style.display = 'block';
	this._startDraw();
};

cssAnimation.prototype._startFPSMeter = function() {
    FPSMeter.run(1);
    this.fpsMeterRunning = true;
};

cssAnimation.prototype._stopFPSMeter = function() {
    if (this.fpsMeterRunning) {
        FPSMeter.stop();
        this.fpsMeterRunning = false;
    }
};

cssAnimation.prototype._startDraw = function() {
    this._startFPSMeter();
    var container = document.getElementById('container');
    var elementsCounter = document.getElementById('elementsCounter');

    // Default configuration
    var numCircles = document.getElementById('numCircles').value || 300;
    var minSize = document.getElementById('minSize').value || 50;
    var maxSize = document.getElementById('maxSize').value || 300;

    var interval = setInterval(function () {
        if (this.count <= numCircles && this.continue) {
            var size = this._getRandomSize(minSize, maxSize);
            var element = document.createElement('div');
            element.className = 'circle';
            element.style.backgroundColor = this._getRandomColor();
            element.style.width  = size;
            element.style.height = size;
            element.style.top    = this._getRandomVerticalPosition(parseInt(size));
            element.style.left   = 0 + 'px';
            element.style[this.supportedTransform] = 'translateX(0px)';
            container.appendChild(element);
            elementsCounter.innerText = this.count++;
            if (this.count == numCircles) {
                document.getElementById('continueButton').style.display = 'none';
            }
            this.showResultsTimeout && clearTimeout(this.showResultsTimeout);
        } else {
            clearInterval(interval);
            this.showResultsTimeout = setTimeout(function() {
                if (!this.nuclearWeaponsOn) {
                    container.innerHTML = '';
                    window.stats.finish();
                }
            }.bind(this), 5000);
        }
    }.bind(this), 25);
};

cssAnimation.prototype.restartExperiment = function() {
    this.continue = false;
    this._stopFPSMeter();
    document.getElementById('continueButton').style.display = 'none';
    document.getElementById('stopButton').style.display = 'none';
    document.getElementById('elementsCounter').innerText = '0';
    document.getElementById('fpsCounter').innerText = '0';
    document.getElementById('container').innerHTML = '';
    document.getElementById('startButton').style.display = 'block';
};

cssAnimation.prototype._getRandomColor = function() {
    // taken from: http://www.paulirish.com/2009/random-hex-color-code-snippets/
    return '#' + Math.floor(Math.random() * 16777215).toString(16);
};

cssAnimation.prototype._getRandomSize = function(min, max) {
    return (Math.random() * (max - min) + min) + 'px';
};

cssAnimation.prototype._getRandomVerticalPosition = function(height) {
    return this.size.height <= height ? 0 + 'px' : Math.random() * (this.size.height - height) + 'px';
};

cssAnimation.prototype.stopGenerateCircles = function(ev) {
    this._stopFPSMeter();
	this.continue = false;
	ev.target.style.display = 'none';
	document.getElementById('continueButton').style.display = 'block';
};

cssAnimation.prototype.continueGenerateCircles = function(ev) {
	this.continue = true;
	ev.target.style.display = 'none';
	document.getElementById('stopButton').style.display = 'block';
    this._startDraw();
};