/**
 * Created by Rok on 21.8.2015.
 */

// Test to see if the code is running on a touch device
function jsAnimation(legendId) {
    var isTouchSupported = 'ontouchstart' in window.document;
    this.defaultValues = {
        numCircles: '300',
        minSize: '50',
        maxSize: '300',
        scaleMinSize: '30',
        scaleMaxSize: '350'
    };
    this.continue = true;
    this.count = 0;
    this.animationTime = 3; // in seconds
    this.fpsMeterRunning = false;
    this.events = {
        onDown : isTouchSupported ? 'touchstart' : 'mousedown',
        onMove : isTouchSupported ? 'touchmove' : 'mousemove',
        onEnd  : isTouchSupported ? 'touchend' : 'mouseup'
    };
    this.size = {
        width: document.body.clientWidth,
        height: document.body.clientHeight
    };

    this._generateLegend(legendId);
    this._registerEventListeners();
    window.stats = new Stats('deviceInfo', 'container', this.defaultValues);
}

jsAnimation.prototype._registerEventListeners = function() {
    document.getElementById('startButton').addEventListener(this.events.onDown, this.start.bind(this));
    document.getElementById('stopButton').addEventListener(this.events.onDown, this.stopGenerateCircles.bind(this));
    document.getElementById('continueButton').addEventListener(this.events.onDown, this.continueGenerateCircles.bind(this));
    document.getElementById('restartButton').addEventListener(this.events.onDown, this.restartExperiment.bind(this));

    if (!window.FPSMeter) {
        throw new Error('FPSMeter is not present in window scope');
    } else {
        document.addEventListener('fps', function(ev) { document.getElementById('fpsCounter').innerText = ev.fps; });
    }
};

jsAnimation.prototype._generateLegend = function(legendId) {
    var html = '<span id="fpsCounter">0</span> FPS<br />' +
        '<span id="elementsCounter">0</span> elements on screen' +
        '<hr>' +
        'Enter details:<br />' +
        '<input type="text" id="deviceInfo" placeholder="Test device">' +
        '<input type="text" id="numCircles" placeholder="Number of circles">' +
        '<input type="text" id="minSize" placeholder="Min Size">' +
        '<input type="text" id="maxSize" placeholder="Max Size">' +
        '<input type="text" id="scaleMinSize" placeholder="Scale min size">' +
        '<input type="text" id="scaleMaxSize" placeholder="Scale max size">' +
        '<button id="startButton" style="float: left">Start</button>' +
        '<button id="stopButton" style="float: left; display: none">Stop</button>' +
        '<button id="continueButton" style="float: left; display: none">Continue</button>' +
        '<button id="restartButton" style="float: right">Restart</button>';

    document.getElementById(legendId).innerHTML = html;
};

jsAnimation.prototype.start = function(ev) {
	this.count = 0;
	this.continue = true;
	ev.target.style.display = 'none';
	document.getElementById('stopButton').style.display = 'block';
	this._startDraw();
};

jsAnimation.prototype._startFPSMeter = function() {
    FPSMeter.run(1);
    this.fpsMeterRunning = true;
};

jsAnimation.prototype._stopFPSMeter = function() {
    if (this.fpsMeterRunning) {
        FPSMeter.stop();
        this.fpsMeterRunning = false;
    }
};

jsAnimation.prototype._startDraw = function() {
    this._startFPSMeter();
    var container = document.getElementById('container');
    var elementsCounter = document.getElementById('elementsCounter');

    // Default configuration
    this.defaultValues.scaleMinSize = document.getElementById('scaleMinSize').value || this.defaultValues.scaleMinSize;
    this.defaultValues.scaleMaxSize = document.getElementById('scaleMaxSize').value || this.defaultValues.scaleMaxSize;

    var numCircles = document.getElementById('numCircles').value || 300;
    var minSize = document.getElementById('minSize').value || 50;
    var maxSize = document.getElementById('maxSize').value || 300;

    var interval = setInterval(function () {
        if (this.count <= numCircles && this.continue) {
            var size = this._getRandomSize(minSize, maxSize);
            var element = document.createElement('div');
            element.className = 'circle';
            element.style.backgroundColor = this._getRandomColor();
            element.style.width  = size;
            element.style.height = size;
            element.style.top    = this._getRandomPx(this.defaultValues.scaleMaxSize, 'top');
            element.style.left   = this._getRandomPx(this.defaultValues.scaleMaxSize, 'left');
            container.appendChild(element);
            elementsCounter.innerText = this.count++;
            this._animate(element);
            if (this.count == numCircles) {
                document.getElementById('continueButton').style.display = 'none';
            }
            this.showResultsTimeout && clearTimeout(this.showResultsTimeout);
        } else {
            clearInterval(interval);
            this.showResultsTimeout = setTimeout(function() {
                container.innerHTML = '';
                window.stats.finish();
            }, 5000);
        }
    }.bind(this), 25);
};

jsAnimation.prototype._animate = function(element) {
    var reverse = false;
    var size = parseInt(element.style.width);
    var maxSize = this.defaultValues.scaleMaxSize;
    var minSize = this.defaultValues.scaleMinSize;
    var stepFirstTime = parseFloat(((maxSize - size) / (this.animationTime * 60)));
    var step = parseFloat(((maxSize - minSize) / (this.animationTime * 60)));
    var moveElement = function(element, firstTime) {
        element = element instanceof Element ? element : element.target;
        if (reverse) {
            if (element.style.width == minSize + 'px' && element.style.height == minSize + 'px') {
                reverse = false;
                size += step;
                element.style.width = size + 'px';
                element.style.height = size + 'px';
            } else {
                if (size - step < minSize) {
                    element.style.width = minSize + 'px';
                    element.style.height = minSize + 'px';
                } else {
                    size -= step;
                    element.style.width = size + 'px';
                    element.style.height = size + 'px';
                }
            }
        } else {
            if (element.style.width == maxSize + 'px' && element.style.height == maxSize + 'px') {
                reverse = true;
                if (firstTime) {
                    firstTime = false;
                }
                size -= step;
                element.style.width = size + 'px';
                element.style.height = size + 'px';
            } else {
                if (size + (firstTime ? stepFirstTime : step) > maxSize) {
                    element.style.width = maxSize + 'px';
                    element.style.height = maxSize + 'px';
                } else {
                    size += (firstTime ? stepFirstTime : step);
                    element.style.width = size + 'px';
                    element.style.height = size + 'px';
                }
            }
        }
        requestAnimationFrame(function(){ moveElement(element, firstTime); });
    };
    requestAnimationFrame(function(){ moveElement(element, true); });
};

jsAnimation.prototype.restartExperiment = function() {
    this.continue = false;
    this._stopFPSMeter();
    document.getElementById('continueButton').style.display = 'none';
    document.getElementById('stopButton').style.display = 'none';
    document.getElementById('elementsCounter').innerText = '0';
    document.getElementById('fpsCounter').innerText = '0';
    document.getElementById('container').innerHTML = '';
    document.getElementById('startButton').style.display = 'block';
};

jsAnimation.prototype._getRandomColor = function() {
    // taken from: http://www.paulirish.com/2009/random-hex-color-code-snippets/
    return '#' + Math.floor(Math.random() * 16777215).toString(16);
};

jsAnimation.prototype._getRandomSize = function(min, max) {
    return (Math.random() * (max - min) + min) + 'px';
};

jsAnimation.prototype._getRandomPx = function(heightOrWidth, offset) {
    var randomPx = null;
    if (offset == 'top') {
        var height = heightOrWidth;
        randomPx = this.size.height <= height ? 0 + 'px' : Math.random() * (this.size.height - height) + 'px';
    } else if (offset == 'left') {
        var width = heightOrWidth;
        randomPx = this.size.width <= width ? 0 + 'px' : Math.random() * (this.size.width - width) + 'px';
    }
    return randomPx;
};

jsAnimation.prototype.stopGenerateCircles = function(ev) {
    this._stopFPSMeter();
	this.continue = false;
	ev.target.style.display = 'none';
	document.getElementById('continueButton').style.display = 'block';
};

jsAnimation.prototype.continueGenerateCircles = function(ev) {
	this.continue = true;
	ev.target.style.display = 'none';
	document.getElementById('stopButton').style.display = 'block';
    this._startDraw();
};