function Stats(deviceName, resultsDivId, additionalMeasureData) {
    if (typeof additionalMeasureData !== 'undefined' && !(additionalMeasureData instanceof Object)) {
        throw new Error('Third parameter must be an object');
    }
    this.fpsPerSec = new Array();
    this.fpsPerHalfSec = new Array();
    this.finished = false;
    this.resultsDiv = document.getElementById(resultsDivId);
    this.deviceName = document.getElementById(deviceName);
    this.additionalMeasureData = additionalMeasureData;
    this.ua = 'User-agent: ' + navigator.userAgent;
}

Stats.prototype._obtainAdditionalMeasureData = function(additionalMeasureData) {
    var additionalMeasureDataValues = {};
    for (var prop in additionalMeasureData) {
        var inputEl = document.getElementById(prop);
        if (!inputEl) {
            throw new Error('Unable to find an element with this ID: ' + prop);
        }
        var value = inputEl.value !== '' ? inputEl.value : additionalMeasureData[prop];
        additionalMeasureDataValues[prop] = value;
    }
    return additionalMeasureDataValues;
};

Stats.prototype.storeStats = function(stats, timeRate) {
    if (!this.finished) {
        if (timeRate == '0.5') {
            this.fpsPerHalfSec.push(stats);
            if (this.fpsPerHalfSec.length == 2) {
                fpsPerSec.push((this.fpsPerHalfSec[0] + this.fpsPerHalfSec[1] / 2));
                this.fpsPerHalfSec = [];
            }
        } else if (timeRate == '1') {
            this.fpsPerSec.push(stats);
        } else {
            throw new Error('Only 1s and 0.5 are supported');
        }
    }
};

Stats.prototype.finish = function() {
    if (this.fpsPerHalfSec.length !== 0) {
        console.warn('Last half sec stats are left out!');
    }
    this._outputResults();
};

Stats.prototype._outputResults = function() {
    this.resultsDiv.innerHTML += 'Benchmark results: <br />' + this.ua + '<br />';
    this.fpsPerSec.forEach(function(statPerSec) {
        this.resultsDiv.innerHTML += statPerSec + '<br />';
    }.bind(this));

    var submitStats = document.createElement('button');
    submitStats.textContent = 'Submit stats';
    this.resultsDiv.appendChild(submitStats);
    submitStats.addEventListener('ontouchstart' in window.document ? 'touchstart' : 'mousedown', this._postResults.bind(this));
    FPSMeter.stop();
};

Stats.prototype._postResults = function(ev) {
    var ua = 'User-agent: ' + navigator.userAgent;
    var stats = '';
    this.fpsPerSec.forEach(function(statPerSec) {
        stats +=  statPerSec + '\n';
    });

    var additionalMeasureData = this._obtainAdditionalMeasureData(this.additionalMeasureData);
    var additionalMeasureDataValues = '';
    for (prop in additionalMeasureData) {
        additionalMeasureDataValues += prop + ': ' + additionalMeasureData[prop] + '\n';
    }
    var http = new XMLHttpRequest();
    var url = '../postStats.php';
    var params = 'deviceName=' + this.deviceName.value + '&ua=' + ua + '&stats=' + stats + '&additionalMeasureData=' + additionalMeasureDataValues;
    http.open('POST', url, true);
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    http.onreadystatechange = function() {
        if (http.readyState == 4 && http.status == 200) {
            alert(http.responseText);
            ev.target.disabled = true;
        } else if (http.readyState == 4) {
            alert('Error #' + http.status + ': ' + http.responseText);
        }
    };
    http.send(params);
};