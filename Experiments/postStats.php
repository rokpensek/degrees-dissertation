<?php
ini_set('display_errors', 1);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $handle = fopen('./stats/' . date('l jS \of F Y h:i:s A') . '.txt', 'w');
    $deviceName = $_POST['deviceName'] ? $_POST['deviceName'] : 'Undefined';
    fwrite($handle, 'Device name: ' . $deviceName . PHP_EOL);
    fwrite($handle, $_POST['ua'] . PHP_EOL );
    fwrite($handle, 'Additional measure data:' . PHP_EOL . $_POST['additionalMeasureData'] . PHP_EOL);
    fwrite($handle, 'FPS stats:' . PHP_EOL . $_POST['stats']);
    echo 'Stats submitted successfully!';
} else {
    echo 'Only POST methods allowed';
}