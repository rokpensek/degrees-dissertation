# Comparison between animations achieved through JS and CSS3 #
## Degree's dissertation

### Overview? ###

Tests are made and grouped with combination of several technologies

* CSS and JS
* JS
* jQuery

Each group consists of three tests that check for **X-Y**, **Scale** and **Color** manipulation.
They include a built-in FPS counter along with an option to submit results.

### How do I get set up? ###

* Clone repository
* Set-up PHP in order to enable submitting results
* Run on mobile / desktop devices (mobile ready)

### Known issues ###

Built-in FPS meter is not reliable when it comes to detecting and evaluating FPS on animations done on composite layers (GPU enhanced). CSS-transitions in particular fall into this category. All tests in the first column use these.
It is better to fallback to Developer tools - Timeline or its built-in FPS counter (Chrome).